//
//  NoteCollectionViewCell.swift
//  notesapp
//
//  Created by John Katt on 4/12/19.
//  Copyright © 2019 JohnKatt. All rights reserved.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell, UICollectionViewDelegateFlowLayout {
    

    @IBOutlet weak var noteTitle: UILabel!
    @IBOutlet weak var noteShort: UILabel!
    
}
