//
//  NoteEditNewView.swift
//  notesapp
//
//  Created by John Katt on 4/12/19.
//  Copyright © 2019 JohnKatt. All rights reserved.
//

import UIKit
import Firebase

class NoteEditNewView: UIViewController {
    
    
    @IBOutlet weak var closeNoteEditView: UIButton!

    @IBOutlet weak var noteTitleLabel: UILabel!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var saveUpdate: UIButton!
    @IBOutlet weak var noteTitleEdit: UITextField!
    @IBOutlet weak var deleteNote: UIButton!
    var ref: DatabaseReference!
    let userid = Auth.auth().currentUser?.uid ?? "none"
    var mainView: NotesViewController?
    let noteGet = getNotes()
    
    override func viewWillAppear(_ animated: Bool) {
        if NotesViewController.whichSeg == "create" {
          noteTitleLabel.isHidden = true
          saveUpdate.setTitle("Create Note", for: .normal)
            deleteNote.isHidden = true
        }
        else {
            noteTitleEdit.isHidden = true
            saveUpdate.setTitle("Edit Note", for: .normal)
            noteTitleLabel.text = NotesViewController.passedTitle
            noteTextView.text = NotesViewController.passedBody
            noteTextView.isUserInteractionEnabled = false
            saveUpdate.isHidden = true
            noteTitleLabel.isUserInteractionEnabled = true
            noteTextView.isUserInteractionEnabled = true
            let titleTap = UITapGestureRecognizer(target: self, action: #selector(NoteEditNewView.titleTapped))
            let noteTap = UITapGestureRecognizer(target: self, action: #selector(NoteEditNewView.noteTapped))
            noteTitleLabel.addGestureRecognizer(titleTap)
            noteTextView.addGestureRecognizer(noteTap)
        }
    }
    
    @objc func titleTapped() {
        self.noteTitleLabel.isHidden = true
        self.noteTitleEdit.placeholder = NotesViewController.passedTitle
        self.noteTitleEdit.isHidden = false
        saveUpdate.isHidden = false
        self.noteTitleEdit.text = NotesViewController.passedTitle
    }
    
    @objc func noteTapped() {
        self.noteTextView.isUserInteractionEnabled = true
        saveUpdate.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
    }
    
    @IBAction func closeNoteView(_ sender: Any) {
        performSegue(withIdentifier: "backHome", sender: Any?.self)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "backHome" {
//            print("happening before segue")
//            if let vc = segue.destination as? NotesViewController {
//                DispatchQueue.main.async {
//                    self.noteGet.mainView = vc
//                    self.noteGet.getData()
//                }
//
//            }
//        }
//
//    }
    @IBAction func newOrEdit(_ sender: Any) {
        
        ref = Database.database().reference()
        if NotesViewController.whichSeg == "create"{
            let noteTitle = noteTitleEdit.text
            let noteBody = noteTextView.text
            
            
            
            ref.child("users").child(userid).observeSingleEvent(of: .value, with: { (snapshot) in
                
                let userDict = snapshot.value as? [String: Any]
                let count = userDict!["total_notes"] as? Int
                let current_count = userDict!["current_notes"] as? Int
                let new_current = current_count! + 1
                let new_count = count! + 1
                self.ref.child("users").child(self.userid).updateChildValues(["total_notes": new_count])
                self.ref.child("users").child(self.userid).updateChildValues(["current_notes": new_current])
                
            })
            let key = ref.child("users").child(userid).childByAutoId().key
            let post = ["uid": userid,
                        "title": noteTitle,
                        "body": noteBody,
                        "active": "yes"]
            let childUpdates = ["/users/\(userid)/notes/\(key)/": post]
            ref.updateChildValues(childUpdates)
            NotesViewController.newChange = true
            performSegue(withIdentifier: "backHome", sender: Any?.self)
        }
        else {
            let noteTitle = noteTitleEdit.text
            let noteBody = noteTextView.text
            ref.child("users").child(userid).child("notes").queryOrdered(byChild: "title").queryEqual(toValue: NotesViewController.passedTitle).observeSingleEvent(of: .value, with: { (snapshot) in
            //ref.child("users").child(userid).child("notes").observeSingleEvent(of: .value, with: { (snapshot) in
                for rest in snapshot.children.allObjects as! [DataSnapshot]{
                    let key = rest.key
                    let child = self.ref.child("users").child(self.userid).child("notes").child(key)
                    let actives = ["title": noteTitle, "body": noteBody]
                    child.updateChildValues(actives, withCompletionBlock: { (err, ref) in
                        
                        if err != nil {
                            return
                        }
                        // add popup saying that the item was in your lists and re favorited!! print("this is title equal and active no")
                        
                    })
                    
                    print(key)
                }
                
                
            })
            NotesViewController.newChange = true
            performSegue(withIdentifier: "backHome", sender: Any?.self)
        }
       // getNotes()
    }
    
    @IBAction func deleteNote(_ sender: Any) {
        ref = Database.database().reference()
        ref.child("users").child(userid).child("notes").queryOrdered(byChild: "title").queryEqual(toValue: NotesViewController.passedTitle).observeSingleEvent(of: .value, with: { (snapshot) in
            //ref.child("users").child(userid).child("notes").observeSingleEvent(of: .value, with: { (snapshot) in
            for rest in snapshot.children.allObjects as! [DataSnapshot]{
                let key = rest.key
                let child = self.ref.child("users").child(self.userid).child("notes").child(key)
                let actives = ["active": "no"]
                child.updateChildValues(actives, withCompletionBlock: { (err, ref) in
                    
                    if err != nil {
                        return
                    }
                    // add popup saying that the item was in your lists and re favorited!! print("this is title equal and active no")
                    
                })
                
                print(key)
            }
            
            
        })
        ref.child("users").child(userid).observeSingleEvent(of: .value, with: { (snapshot) in
            let userDict = snapshot.value as? [String: Any]
            let count = userDict!["current_notes"] as? Int
            let new_count = count! - 1
            self.ref.child("users").child(self.userid).updateChildValues(["current_notes": new_count])
        })
        performSegue(withIdentifier: "backHome", sender: Any?.self)
    }
    
    
    
//    @IBAction func editNote(_ sender: Any) {
//        DispatchQueue.main.async {
//        self.noteTitleLabel.isHidden = true
//        self.noteTitleEdit.isHidden = false
//        self.saveUpdate.isHidden  = false
//        self.editNote.isHidden = true
//        self.deleteNote.isHidden = true
//        self.noteTitleEdit.placeholder = NotesViewController.passedTitle
//        self.noteTextView.isUserInteractionEnabled = true
//        super.viewDidLoad()
//        }
//
//    }
    
}
