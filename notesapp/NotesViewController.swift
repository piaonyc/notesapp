//
//  ViewController.swift
//  notesapp
//
//  Created by John Katt on 4/12/19.
//  Copyright © 2019 JohnKatt. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class NotesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var noteCollection: UICollectionView!

    static var whichSeg = ""
    static var passedTitle = ""
    static var passedBody = ""
    static var passedNoteID = ""
    static var newChange = false
    let getNote = getNotes()
    let editNew = NoteEditNewView()
    let userid = Auth.auth().currentUser?.uid ?? "none"
    
    var roundButton = UIButton()
    
    override func viewWillAppear(_ animated: Bool) {
       print(getNotes.titleArray)
       getNote.mainView = self
       getNote.getData()
       createFloatingButton()
        
    }
  
    override func viewDidLoad() {
        print("Here in main",  getNotes.countArray)
        super.viewDidLoad()
        noteCollection.delegate = self
        noteCollection.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        //layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: width / 1.01, height: width / 2)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        noteCollection!.collectionViewLayout = layout
        noteCollection.reloadData()
     }
    
    override func viewDidAppear(_ animated: Bool) {
        if NotesViewController.newChange {
            getNote.mainView = self
            getNote.getData()
            DispatchQueue.main.async {
                self.noteCollection.collectionViewLayout.invalidateLayout()
                self.noteCollection.reloadData()
            }
            NotesViewController.newChange = false
        }
        else {
            noteCollection.collectionViewLayout.invalidateLayout()
            noteCollection.reloadData()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        let counter = getNotes.countArray
        if counter > 0 {
            count = getNotes.countArray
        }
        else {
            count = 0
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let noteCell = noteCollection.dequeueReusableCell(withReuseIdentifier: "noteCell", for: indexPath) as! NoteCollectionViewCell
        
        
        if getNotes.titleArray == [] {
            noteCell.noteTitle.text = ""
            print("empty")
            return noteCell
        }
        else {
            let previewBody = String(getNotes.bodyArray[indexPath.row].prefix(40))

            noteCell.noteTitle.text = getNotes.titleArray[indexPath.row]
            noteCell.noteShort.text = previewBody
            return noteCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotesViewController.whichSeg = "passing"
        NotesViewController.passedTitle = getNotes.titleArray[indexPath.row]
        NotesViewController.passedBody = getNotes.bodyArray[indexPath.row]
        roundButton.isHidden = true
        editNew.mainView = self
        performSegue(withIdentifier: "createNote", sender: Any?.self)
    }
    
    
    
    
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = .white
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"icons8-plus-math-filled-40.png"), for: .normal)
        roundButton.imageView?.layer.cornerRadius = 37.5
        //roundButton.imageView?.clipsToBounds = true
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(addEditNote), for: .touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 15),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 75),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 75)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 37.5
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
        }
    }

    @objc func addEditNote() {
        NotesViewController.whichSeg = "create"
        roundButton.isHidden = true
        performSegue(withIdentifier: "createNote", sender: Any?.self)
        editNew.mainView = self
        print("go to next screen")
    }
    
  
    
    
    @IBAction func logoutAction(_ sender: Any) {
        try! Auth.auth().signOut()
        self.performSegue(withIdentifier: "logout", sender: Any?.self)
        
    }
    
    
}

