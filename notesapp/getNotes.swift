//
//  getNotes.swift
//  notesapp
//
//  Created by John Katt on 4/15/19.
//  Copyright © 2019 JohnKatt. All rights reserved.
//

import Firebase
import FirebaseAuth
import UIKit

class getNotes: NSObject {
    var ref: DatabaseReference!
    static var titleArray = [String] ()
    static var bodyArray = [String] ()
    var uidArray = [String] ()
    static var countArray = 0
    var mainView: NotesViewController?
    
    func getData() {
        getNotes.titleArray.removeAll()
        getNotes.bodyArray.removeAll()
        getNotes.countArray = 0
        
        ref = Database.database().reference()
        let userid = Auth.auth().currentUser?.uid ?? "none"
        print("here in data", userid)
        ref?.child("users").child(userid).child("current_notes").observeSingleEvent(of: .value, with: { (snapshot) in
        var count = 0
        count = (snapshot.value as? Int ?? 0)
        getNotes.countArray = (count)
        print("here in get data", getNotes.countArray)
            })

        ref?.child("users").child(userid).child("notes").observeSingleEvent(of: .value, with: { (snapshot) in
        for rest in snapshot.children.allObjects as! [DataSnapshot]{
            guard let restDict = rest.value as? [String: Any] else { continue }
            let active = (restDict["active"] as? String)
            if active == "yes" {
                getNotes.titleArray.append((restDict["title"] as? String ?? "none"))
                getNotes.bodyArray.append(restDict["body"] as? String ?? "none")
                self.uidArray.append(rest.key)
            }
        }
        
       self.mainView!.noteCollection.collectionViewLayout.invalidateLayout()
       self.mainView!.noteCollection.reloadData()

    })
        
        
        
}
    
    
}
