//
//  signupLogin.swift
//  notesapp
//
//  Created by John Katt on 4/15/19.
//  Copyright © 2019 JohnKatt. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignupLoginViewController: UIViewController {
    
   
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton!
     let getNote = getNotes()
    override func viewWillAppear(_ animated: Bool) {
        if Auth.auth().currentUser != nil {
            print("test me")
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "loginSignup", sender: self)
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func loginSignup(_ sender: Any) {
        let email = self.username.text
        let password = self.password.text
        
//       if (email == "") || (password == "") {
//            let alert = UIAlertController(title: "Need to Enter Email and Password", message: "Email and password cannot be blank", preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
//
//            self.present(alert, animated: true)
//        }
        
            let date = NSDate() // Get Todays Date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let currentDateTime: String = dateFormatter.string(from: date as Date)
            Auth.auth().fetchProviders(forEmail: email!, completion: {
                (providers, error) in
                if providers != nil {
                    print("there is account, so we are going to login")
                    Auth.auth().signIn(withEmail: email!, password: password!) { (user, error) in
                        if error != nil {
                            let errorMess = error?.localizedDescription
                            if errorMess == "The password is invalid or the user does not have a password." {
                                let alert = UIAlertController(title: "Wrong Password", message: "You entered the wrong password, please try again", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                                self.present(alert, animated: true)
                            }
                            else if errorMess == "The email address is badly formatted." {
                                let alert = UIAlertController(title: "Unknown Email", message: "Unknown email, if you need to create an account go to the sign up page!", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                                self.present(alert, animated: true)
                            }
                            else {
                                let alert = UIAlertController(title: "Unknown Error", message: "Please contact us!", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                                self.present(alert, animated: true)
                            }
                        }
                        else {
                            var ref: DatabaseReference!
                            ref = Database.database().reference()
                            guard let userid = Auth.auth().currentUser?.uid else { return }
                            ref.child("users").child((userid)).child("logins").updateChildValues([currentDateTime : "email"])
                        }
                        self.performSegue(withIdentifier: "loginSignup", sender: Any?.self)
                    }
                    
                }
                else {
                    print("there is no account, so we are going to create one")
                    
                    Auth.auth().createUser(withEmail: email!, password: password!) { (authResult, error) in
                        // ...
                        guard (authResult?.user) != nil else {
                            
                            let alertController = UIAlertController(title: "User Creation", message:
                                "A User already exists with that email. If you need reset your password", preferredStyle: UIAlertController.Style.alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            return }
                        
                        var ref: DatabaseReference!
                        ref = Database.database().reference()
                        guard let userid = Auth.auth().currentUser?.uid else { return }
                        ref.child("users").child((userid)).child("email").updateChildValues(["email":email!])
                        ref.child("users").child((userid)).child("logins").updateChildValues([currentDateTime : "emailLogin"])
                        ref.child("users").child((userid)).updateChildValues(["current_notes" : 0])
                        ref.child("users").child((userid)).updateChildValues(["total_notes": 0])
                        
                        self.performSegue(withIdentifier: "loginSignup", sender: Any?.self)
                    }
                
                }
            })
        
        }

    }
